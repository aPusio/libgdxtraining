package com.pusio.game.examples.scene2d;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

import java.util.Random;

public class E5ActorHit implements ApplicationListener{
    class Jet extends Actor{
        private TextureRegion texture;

        public Jet(TextureRegion texture) {
            this.texture = texture;

            setBounds(getX(), getY(), texture.getRegionWidth(), texture.getRegionHeight());

            this.addListener(new InputListener(){
                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    System.out.println("Touched: " + getName());
                    setVisible(false);
                    return true;
                }
            });
        }

        @Override
        public void draw(Batch batch, float parentAlpha) {
            batch.draw(texture, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
        }

        @Override
        //used by touchDown
        //there is no need to reload this function
        //default function works even better!
        public Actor hit(float x, float y, boolean touchable) {

            if(!this.isVisible() || this.getTouchable() == Touchable.disabled){
                return null;
            }

            //center points of bounding circle
            float centerX = getWidth() /2;
            float centerY = getHeight()/2;

            // Calculate radius of circle
            float radius = (float) Math.sqrt(centerX * centerX +
                    centerY * centerY);

            // And distance of point from the center of the circle
            float distance = (float) Math.sqrt(((centerX - x) * (centerX - x))
                    + ((centerY - y) * (centerY - y)));

            // If the distance is less than the circle radius, it's a hit
            if(distance <= radius) return this;

            // Otherwise, it isnt
            return null;

        }
    }

    private Jet jets[];
    private Stage stage;

    @Override
    public void create() {
        stage = new Stage(new ExtendViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
        final TextureRegion jetTexture = new TextureRegion(new Texture(Gdx.files.internal("jet.png")));
        jets = new Jet[10];

        Random random = new Random();

        for(int i=0; i<10; i++){
            jets[i] = new Jet(jetTexture);

            jets[i].setPosition(random.nextInt(Gdx.graphics.getWidth() - (int)jets[i].getWidth())
                    , random.nextInt(Gdx.graphics.getHeight() - (int)jets[i].getHeight()));

            jets[i].setName(Integer.toString(i));
            stage.addActor(jets[i]);
        }
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}

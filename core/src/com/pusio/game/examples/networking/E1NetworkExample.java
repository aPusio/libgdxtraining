package com.pusio.game.examples.networking;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.net.ServerSocket;
import com.badlogic.gdx.net.ServerSocketHints;
import com.badlogic.gdx.net.Socket;
import com.badlogic.gdx.net.SocketHints;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextArea;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

public class E1NetworkExample implements ApplicationListener {
    private OrthographicCamera camera;
    private SpriteBatch batch;
    private Skin skin;
    private Stage stage;
    private Label labelDetails;
    private Label labelMessage;
    private TextButton button;
    private TextArea textIPAddress;
    private TextArea textMessage;

    // Pick a resolution that is 16:9 but not unreadibly small
    public final static float VIRTUAL_SCREEN_HEIGHT = 800;
    public final static float VIRTUAL_SCREEN_WIDTH = 480;

    @Override
    public void create() {
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        batch = new SpriteBatch();

        skin = new Skin(Gdx.files.internal("uiskin.json"));
        stage = new Stage();

        Gdx.input.setInputProcessor(stage);

        //loop throught avialable network interfaces
        List<String> addresses = new ArrayList<>();

        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            for (NetworkInterface ni : Collections.list(interfaces)) {
                for (InetAddress address : Collections.list(ni.getInetAddresses())) {
                    if (address instanceof Inet4Address) {
                        addresses.add(address.getHostAddress());
                    }
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }

        StringBuilder addressBuilder = new StringBuilder();
        for (String address : addresses) {
            addressBuilder.append(address + "\n");
        }
        String ipAddress = addressBuilder.toString();

        VerticalGroup verticalGroup = new VerticalGroup().space(3).pad(5).fill();
        verticalGroup.setBounds(0, 0, VIRTUAL_SCREEN_WIDTH, VIRTUAL_SCREEN_HEIGHT);

        labelDetails = new Label(ipAddress, skin);
        labelMessage = new Label("Hello World", skin);
        button = new TextButton("Send message", skin);
        textIPAddress = new TextArea("", skin);
        textMessage = new TextArea("", skin);

        verticalGroup.addActor(labelDetails);
        verticalGroup.addActor(labelMessage);
        verticalGroup.addActor(button);
        verticalGroup.addActor(textIPAddress);
        verticalGroup.addActor(textMessage);

        stage.addActor(verticalGroup);
//        stage.setViewport(new ExtendViewport(VIRTUAL_SCREEN_WIDTH, VIRTUAL_SCREEN_HEIGHT));
//        stage.getCamera().position.set(VIRTUAL_SCREEN_WIDTH / 2, VIRTUAL_SCREEN_HEIGHT / 2, 0);
        stage.getCamera().position.set(VIRTUAL_SCREEN_WIDTH / 2, VIRTUAL_SCREEN_HEIGHT / 2, 0);
        stage.getCamera().viewportWidth = VIRTUAL_SCREEN_WIDTH;
        stage.getCamera().viewportHeight = VIRTUAL_SCREEN_HEIGHT;

        //Thread for incoming socket connections
        new Thread(new Runnable() {
            @Override
            public void run() {
                ServerSocketHints serverSocketHints = new ServerSocketHints();
                //0 means no timeonu
                serverSocketHints.acceptTimeout = 0;

                //create socket server that listen on port 9021
                ServerSocket serverSocket = Gdx.net.newServerSocket(Net.Protocol.TCP, 9021, serverSocketHints);

                //infinity loop
                while (true){
                    //create socket
                    Socket socket = serverSocket.accept(null);

                    BufferedReader buffer = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                    try {
                        labelMessage.setText(buffer.readLine());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
        }).start();

        button.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {

                String textToSend;
                textToSend = textMessage.getText().length() == 0 ? "button was clicked without message\n" : textMessage.getText() + "\n";

                SocketHints socketHints = new SocketHints();

                socketHints.connectTimeout = 4000;
                Socket socket = Gdx.net.newClientSocket(Net.Protocol.TCP, textIPAddress.getText(), 9021, socketHints);

                try {
                    socket.getOutputStream().write(textToSend.getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0.5f, 0.5f, 0.5f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        stage.draw();
        batch.end();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        batch.dispose();
    }
}

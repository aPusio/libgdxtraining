package com.pusio.game.examples.drawing;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.StringBuilder;
import com.badlogic.gdx.utils.Timer;

public class E3DrawingSprite implements ApplicationListener {
    private SpriteBatch batch;
    private TextureAtlas textureAtlas;
    private Sprite sprite;
    private int currentFrame = 1;
    private String currentAtlasKey = "0001";

    @Override
    public void create() {
        batch = new SpriteBatch();
        textureAtlas = new TextureAtlas(Gdx.files.internal("spritesheet.atlas"));
        TextureAtlas.AtlasRegion region = textureAtlas.findRegion("0001");
        sprite = new Sprite(region);
        sprite.setPosition(120,100);
        sprite.scale(2.5f);
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                if(++currentFrame>20){
                    currentFrame = 1;
                }
                //this is LIBGDX builder !
                currentAtlasKey = new StringBuilder().append(currentFrame, 4).toString();
                sprite.setRegion(textureAtlas.findRegion(currentAtlasKey));
            }
        },0,1/30f);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0,0,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        sprite.draw(batch);
        batch.end();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        textureAtlas.dispose();
    }
}
